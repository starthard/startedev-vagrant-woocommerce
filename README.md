# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Configuração do projeto ###

O projeto usa a arquitetura do Spring Boot e o controle de dependencia Gradle por isso são necessarios alguns passos para que se possa programar usando o Eclipse

1. Clone o projeto para a sua maquina local
1. Entre na pasta raiz do projeto
1. Use o SHIFT + botão esquerdo do mouse e escolha a opção "Abrir janela de comando aqui"
1. digite o comando "gradlew eclipse"
1. o Gradle ira converter o projeto compatível para o Eclipse, depois e só importar o projeto normalmente

# Arquitetura do projeto #

### Spring Boot ###

O projeto usa todas as tecnologias presentes do Spring Boot isso inclui a camada do Controller de Serviço e DAO
Link de referencia: http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/

### Thymeleaf ###

O Thymeleaf é um template engine que substitui o uso do JSP e tem integração com o Spring 
Com ele é possivel trabalhar de forma parecida com o VRaptor onde o retorno da chamada do Controller é o nome do arquivo HTML da pagina 
Link de referencia: http://www.thymeleaf.org/doc/tutorials/2.1/usingthymeleaf.html

### Camadas ###

Vamos diminuir o numero da camada para 3 Controller/Domain/DAO e eliminar o uso do DTO e Service no inicio, agora essas duas camadas vão passar a ser a Domain seguindo a ideia do DDD(Domain Driven Design) assim o projeto não vai ficar com interface inúteis que não são usadas nunca e o projeto fica mais enxuto no inicio (possivelmente por causa do escopo do projeto vai ser necessário criar alguns Service mais no momento não vamos utilizar para se acostumar com esse tipo de design de projeto)
Link de referencia: http://www.agileandart.com/2010/07/16/ddd-introducao-a-domain-driven-design/